import java.util.Formatter;

public class CreateFile {

	private Formatter x;
	
	public void openFile() {
		try {
			x = new Formatter("booking.txt");
		}
		
		catch(Exception e) {
			System.out.println("You have an error opening the file! ");
		}
		
	}
	
	public void addRecords() {
		
		x.format("%s,%s,%s","Hotel California","Hotel Dublin", "Hotel Oradea");
	}
	
	public void closeFile() {
		
		x.close();
	}
}
