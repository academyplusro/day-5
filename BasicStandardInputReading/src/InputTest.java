import java.io.IOException;

public class InputTest {

	public static void main(String[] args) throws IOException {
		  int ch;
	      System.out.print ("Enter some text: ");
	      while ((ch = System.in.read ()) != '\n')
	         System.out.print ((char) ch);

	}

}
