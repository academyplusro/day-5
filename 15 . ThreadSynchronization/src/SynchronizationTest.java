public class SynchronizationTest {

	public static void main(String[] args) {
		    Sender snd = new Sender();
	        ThreadedSend S1 = new ThreadedSend( "Hi Terry Williams \n " , snd );
	        ThreadedSend S2 = new ThreadedSend(  "We hope you enjoy the stay at our hotel! \n" + " " , snd );
	 
	        // Start two threads of ThreadedSend type
	        S1.start();
	        S2.start();
	 
	        // wait for threads to end
	        try {
	            S1.join();
	            S2.join();
	        }
	        catch(Exception e) {
	            System.out.println("Interrupted");
	        }

	}

}
