import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InputReading {

	public static void main(String[] args)throws IOException{
	
	 int n;
	 
	 System.out.print ("Introduce the number of elements n = ");
	 
	 BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
	 n = Integer.parseInt(stdin.readLine());
	 int V[]=new int[n];
	 int i;
	 for(i = 0; i < n; i++) {
	 System.out.print("V["+i+"]=");
	 V[i]=Integer.parseInt(stdin.readLine());
	 }
	 for(i = 0; i < n; i++)
	 System.out.println(V[i]);
	}

}
